﻿var jjmQuote;
var xrmPage = "";
if (jjmQuote === undefined || workorder === null) {
    jjmQuote = {};
    jjmQuote = (function () {

        var onTypeChange = function (executionContext) {

            xrmPage = executionContext.getFormContext();
            xrmPage.ui.tabs.get("tab_5").setVisible(false);

            let type = xrmPage.getAttribute("ind_type").getValue();
            if (type) {
                if (type === 121430001) {
                    xrmPage.ui.tabs.get("tab_5").setVisible(true);

                }
            }
            xrmPage.data.process.addOnStageSelected(FocusOnTab);
        };

        var FocusOnTab = function () {
            var selectedStage = xrmPage.data.process.getSelectedStage();
            if (selectedStage !== null) {
                //GUID of Quote Approval stage
                if (selectedStage.getId() === "f76bcdaf-e9e6-4702-85c1-b09cc1f77288") {
                    xrmPage.ui.tabs.get("tab_3").setFocus();
                }
                //GUID of Agreement approval stage
                else if (selectedStage.getId() === "6ce6afb9-a957-438e-8590-5bdcb1d1378c") {
                    xrmPage.ui.tabs.get("tab_4").setFocus();
                }
            }
        };

        return {
            OnTypeChange: onTypeChange
        };
    }());

}