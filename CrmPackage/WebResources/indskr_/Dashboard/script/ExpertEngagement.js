﻿var expertEngagement;
if (expertEngagement === undefined || expertEngagement === null) {
    expertEngagement = {};
    expertEngagement = (function () {
        var xrmpage;
        var appid;
        var clientUrl;
        var userSettings;


        var fnOnLoadDashboard = function (context) {
            xrmpage = parent.Xrm.Page;
            appid = xrmpage.context.getCurrentAppUrl().split("?")[1];


            var globalContext = parent.Xrm.Utility.getGlobalContext();
            clientUrl = globalContext.getClientUrl();
            userSettings = globalContext.userSettings;

            $("#username").text(userSettings.userName);

            
            var req = new XMLHttpRequest();
            req.open("POST", clientUrl + "/api/data/v9.1/indskr_iOGetExpertEngagementsDashboardDetails", true);
            req.setRequestHeader("OData-MaxVersion", "4.0");
            req.setRequestHeader("OData-Version", "4.0");
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.onreadystatechange = function () {
                if (this.readyState === 4) {
                    req.onreadystatechange = null;
                    if (this.status === 200) {
                        var results = JSON.parse(this.response);
                        PopulateNumbers(results);
                    } else {
                        parent.Xrm.Utility.alertDialog(this.statusText);
                    }
                }
            };
            req.send();

            console.log("on Load Dashboard ");
        };

        function PopulateNumbers(results) {
            var result = JSON.parse(results.JSONOutput);
            $("#EngagementRequestsInDraft").text(result.EngagementRequestsInDraft);
            $("#EngagementRequestsActive").text(result.EngagementRequestsActive);
            $("#EngagementRequestsInReview").text(result.EngagementRequestsInReview);

            $("#ContractsInReview").text(result.ContractsInReview);
            $("#ExpertEngagementPlan").text(result.ExpertEngagementPlan);
            $("#ExpertsNeeded").text("5");

            PopulateCharts(result.ExpensesDetails);
            PopulateBarChart(result.PaymentDetails);
            parent.Xrm.Utility.closeProgressIndicator();
        }

        function PopulateCharts(data) {
            
            var catagory = [];
            var amount = [];

            for (var i = 0; i < data.length; i++) {
                catagory.push(data[i].Catagory);
                amount.push(data[i].TotalAmount);
            }
            
            //Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
            //Chart.defaults.global.defaultFontColor = '#858796';

            // Pie Chart Example
            var ctx = document.getElementById("myPieChart");
            var myPieChart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    labels: catagory,
                    datasets: [{
                        data: amount,
                        backgroundColor: ['#4e73df', '#1cc88a', '#36b9cc', '#e83e8c'],
                        hoverBackgroundColor: ['#2e59d9', '#17a673', '#2c9faf', '#e83e8c'],
                        hoverBorderColor: "rgba(234, 236, 244, 1)",
                    }],
                },
                options: {
                    maintainAspectRatio: false,
                    tooltips: {
                        backgroundColor: "rgb(255,255,255)",
                        bodyFontColor: "#858796",
                        borderColor: '#dddfeb',
                        borderWidth: 1,
                        xPadding: 15,
                        yPadding: 15,
                        displayColors: false,
                        caretPadding: 10,
                        callbacks: {
                            label: function (tooltipItem, data) {
                                var label = data.datasets[0].data[tooltipItem.index];

                                if (label) {
                                    label += ' $';
                                }
                                
                                return label;
                            }
                        }
                    },
                    legend: {
                        display: true
                    },
                    cutoutPercentage: 1,
                },
            });
        }

        function PopulateBarChart(data) {
            var status = [];
            var count = [];

            for (var i = 0; i < data.length; i++) {
                status.push(data[i].Status);
                count.push(data[i].Count);
            }

            // Set new default font family and font color to mimic Bootstrap's default styling
            //Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
            //Chart.defaults.global.defaultFontColor = '#858796';

            // Bar Chart Example
            var ctx = document.getElementById("myBarChart");
            var myBarChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: status,
                    datasets: [{
                        label: "Count",
                        backgroundColor: "#4e73df",
                        hoverBackgroundColor: "#2e59d9",
                        borderColor: "#4e73df",
                        data: count,
                    }],
                },
                options: {
                    maintainAspectRatio: false,
                    layout: {
                        padding: {
                            left: 10,
                            right: 25,
                            top: 25,
                            bottom: 0
                        }
                    },
                    scales: {
                        xAxes: [{
                            time: {
                                unit: 'Status'
                            },
                            gridLines: {
                                display: true,
                                drawBorder: false
                            },
                            ticks: {
                                maxTicksLimit: 4
                            },
                            maxBarThickness: 50,
                        }],
                        yAxes: [{
                            ticks: {
                                min: 0,
                                maxTicksLimit: 5,
                                padding: 10,
                            },
                            gridLines: {
                                color: "rgb(234, 236, 244)",
                                zeroLineColor: "rgb(234, 236, 244)",
                                drawBorder: false,
                                borderDash: [2],
                                zeroLineBorderDash: [2]
                            }
                        }],
                    },
                    legend: {
                        display: false
                    }
                }
            });

        }

        var fnOnClickCreatePlan = function () {
            parent.Xrm.Utility.openEntityForm("indskr_expertengagementplan");
        }

        var fnOnClickRequest = function () {
            parent.Xrm.Utility.openEntityForm("indskr_expertengagementrequest");
        }

        var fnOnClickExpense = function () {
            parent.Xrm.Utility.openEntityForm("indskr_expense");
        }

        var fnOnClickManagePlans = function () {
            parent.location = clientUrl+"/main.aspx?appid=db4a3ecb-40ae-4ebc-b202-56b1c6db5ea8&forceUCI=1&pagetype=entitylist&etn=indskr_expertengagementplan&viewid=7f2bc448-60b3-44e2-b442-e6bf4cedef28&viewType=1039";
        }

        var fnOnClickManageRequest = function () {
            parent.location = clientUrl + "/main.aspx?appid=db4a3ecb-40ae-4ebc-b202-56b1c6db5ea8&forceUCI=1&newWindow=true&pagetype=entitylist&etn=indskr_expertengagementrequest&viewid=dd7107d7-d176-47a5-8ab2-f0facba06480&viewType=1039";
        }

        var fnOnClickManageExpenses = function () {
            parent.location = clientUrl + "/main.aspx?appid=db4a3ecb-40ae-4ebc-b202-56b1c6db5ea8&forceUCI=1&newWindow=true&pagetype=entitylist&etn=indskr_expense&viewid=39071825-904b-442d-bf30-1eb831d2438d&viewType=1039";
        }

        var fnOnClickSeeRecent = function () {
            parent.location = clientUrl + "/main.aspx?appid=db4a3ecb-40ae-4ebc-b202-56b1c6db5ea8&pagetype=entitylist&etn=indskr_expertengagementactivitypayment&viewid=709043de-259f-ea11-a812-000d3a99ba4c&viewType=1039";
        }

        var fnOnClickManagePayments = function () {
            parent.location = clientUrl + "/main.aspx?appid=db4a3ecb-40ae-4ebc-b202-56b1c6db5ea8&pagetype=entitylist&etn=indskr_expertengagementactivitypayment&viewid=c9ba2042-487c-4e2a-809b-f02c9e894db9&viewType=1039";
        }


        return {
            OnLoadDashboard: fnOnLoadDashboard,
            OnClickCreatePlan: fnOnClickCreatePlan,
            OnClickRequest: fnOnClickRequest,
            OnClickExpense: fnOnClickExpense,
            OnClickManagePlans: fnOnClickManagePlans,
            OnClickManageRequest: fnOnClickManageRequest,
            OnClickManageExpenses: fnOnClickManageExpenses,
            OnClickSeeRecent: fnOnClickSeeRecent,
            OnClickManagePayments: fnOnClickManagePayments
        };
    }());

}