﻿var commercial;
if (commercial === undefined || commercial === null) {
    commercial = {};
    commercial = (function () {

        var clientUrl;
        var userSettings;
        function PopulateChart(lables, dataSet, ctx) {

            var myPieChart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    labels: lables,
                    datasets: [{
                        data: dataSet,
                        backgroundColor: ['#20c9a6', '#CC0000', '#858796', '#4e73df'],
                        hoverBackgroundColor: ['#E57D7D', '#2e59d9','#A0DAC5', '#F9E6AB'],
                        hoverBorderColor: "rgba(234, 236, 244, 1)",
                    }],
                },
                options: {
                    maintainAspectRatio: false,
                    legend: {
                        display: true
                    },
                    cutoutPercentage: 1,
                },
            });
        }

        function CallDashboardDataAction() {
            var req = new XMLHttpRequest();
            req.open("POST", parent.Xrm.Page.context.getClientUrl() + "/api/data/v9.1/indskr_iOGetCommercialDashboardDetails", true);
            req.setRequestHeader("OData-MaxVersion", "4.0");
            req.setRequestHeader("OData-Version", "4.0");
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.onreadystatechange = function () {
                if (this.readyState === 4) {
                    
                    req.onreadystatechange = null;
                    if (this.status === 200) {
                        var results = JSON.parse(this.response);
                        
                        PopulateNumbers(results);
                    } else {
                        parent.Xrm.Utility.alertDialog(this.response);
                    }
                }
            };
            req.send();
        }

        function PopulateNumbers(results) {
            var result = JSON.parse(results.JSONOutput);
            OpportunityChart(result);
            QuoteCharts(result);
            InvoiceCharts(result);
            PopulateBarChart(result.OrderCategories);


            //SpeakerEngagementCharts(result);
            //SpeakerContractCharts(result);

            //$("#Registrations").text(result.RegistrationNumbers);
            //$("#LeadsCaptured").text(result.LeadsCapturedNumbers);

            //$("#Approved").text(result.SpeakerEngagementsCount);
            //$("#Confirmed").text(result.VenuesConfirmedNumbers);
            //$("#Registered").text(result.VendorsRegistrationNumbers);


            parent.Xrm.Utility.closeProgressIndicator();
        }

        function OpportunityChart(result) {
            var data = result.OpportunityCategories;
            var catagory = [];
            var count = [];
            for (var i = 0; i < data.length; i++) {
                catagory.push(data[i].Status);
                count.push(data[i].Count);
            }
            var ctx = document.getElementById("EventPlanChart");
            PopulateChart(catagory, count, ctx);
            $("#EventPlanCategoriesCount").text(result.OpportunityCategoriesCount);
        }

        function QuoteCharts(result) {
            var data = result.QuoteCategories;
            var catagory = [];
            var count = [];
            for (var i = 0; i < data.length; i++) {
                catagory.push(data[i].Status);
                count.push(data[i].Count);
            }
            var ctx = document.getElementById("SpeakerCharts");
            PopulateChart(catagory, count, ctx);
            $("#SpeakerCategoriesCount").text(result.QuoteCategoriesCount);
        }

        function InvoiceCharts(result) {
            var data = result.InvoiceCategories;
            var catagory = [];
            var count = [];
            for (var i = 0; i < data.length; i++) {
                catagory.push(data[i].Status);
                count.push(data[i].Count);
                if (data[i].Status == "Billed") {
                    $("#Billed").text(data[i].Count);
                }
                if (data[i].Status == "Complete") {
                    $("#Complete").text(data[i].Count);
                }
                if (data[i].Status == "New") {
                    $("#New").text(data[i].Count);
                }
            }

            var ctx = document.getElementById("SpeakerCharts");
            //PopulateChart(catagory, count, ctx);
            $("#SpeakerCategoriesCount").text(result.InvoiceCategoriesCount);
        }

        function PopulateBarChart(data) {
            debugger;
            var status = [];
            var count = [];

            for (var i = 0; i < data.length; i++) {
                status.push(data[i].Status);
                count.push(data[i].Count);
            }

            // Set new default font family and font color to mimic Bootstrap's default styling
            //Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
            //Chart.defaults.global.defaultFontColor = '#858796';

            // Bar Chart Example
            var ctx = document.getElementById("myBarChart");
            var myBarChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: status,
                    datasets: [{
                        label: "Count",
                        backgroundColor: "#4e73df",
                        hoverBackgroundColor: "#2e59d9",
                        borderColor: "#4e73df",
                        data: count,
                    }],
                },
                options: {
                    maintainAspectRatio: false,
                    layout: {
                        padding: {
                            left: 10,
                            right: 25,
                            top: 25,
                            bottom: 0
                        }
                    },
                    scales: {
                        xAxes: [{
                            time: {
                                unit: 'Status'
                            },
                            gridLines: {
                                display: true,
                                drawBorder: false
                            },
                            ticks: {
                                maxTicksLimit: 4
                            },
                            maxBarThickness: 50,
                        }],
                        yAxes: [{
                            ticks: {
                                min: 0,
                                maxTicksLimit: 5,
                                padding: 10,
                            },
                            gridLines: {
                                color: "rgb(234, 236, 244)",
                                zeroLineColor: "rgb(234, 236, 244)",
                                drawBorder: false,
                                borderDash: [2],
                                zeroLineBorderDash: [2]
                            }
                        }],
                    },
                    legend: {
                        display: false
                    }
                }
            });

        }

        var fcOnLoadCommercial = function () {
            
            var globalContext = parent.Xrm.Utility.getGlobalContext();
            clientUrl = globalContext.getClientUrl();
            userSettings = globalContext.userSettings;
            $("#username").text(userSettings.userName);
            CallDashboardDataAction();
        };

        var fnOnClickCreateOpportunit = function () {
            parent.location = clientUrl + "/main.aspx?appid=27ba11a2-425d-e911-a829-000d3a1d5713&forceUCI=1&pagetype=entityrecord&etn=opportunity";
        }

        var fnOnClickManageOpportunities = function () {
            parent.location = clientUrl + "/main.aspx?appid=27ba11a2-425d-e911-a829-000d3a1d5713&forceUCI=1&pagetype=entitylist&etn=opportunity&viewid=c6f6fc1b-1b3e-e711-80e1-00155d871bb4&viewType=1039";
        }

        var fnOnClickCreateQuote = function () {
            parent.location = clientUrl + "/main.aspx?appid=27ba11a2-425d-e911-a829-000d3a1d5713&forceUCI=1&pagetype=entityrecord&etn=quote";
        }

        var fnOnClickManageQuote = function () {
            parent.location = clientUrl + "/main.aspx?appid=27ba11a2-425d-e911-a829-000d3a1d5713&forceUCI=1&pagetype=entitylist&etn=quote&viewid=00000000-0000-0000-00aa-000010013100&viewType=1039";
        }

        var fnOnClickCreateOrders = function () {
            parent.location = clientUrl + "/main.aspx?appid=27ba11a2-425d-e911-a829-000d3a1d5713&forceUCI=1&pagetype=entityrecord&etn=salesorder";
        }

        var fnOnClickManageOrders = function () {
            parent.location = clientUrl + "/main.aspx?appid=27ba11a2-425d-e911-a829-000d3a1d5713&forceUCI=1&pagetype=entitylist&etn=salesorder&viewid=c237560a-5dad-e411-80d5-00155dbeee13&viewType=1039";
        }

        var fnOnClickAllInvoices = function () {
            parent.location = clientUrl + "/main.aspx?appid=27ba11a2-425d-e911-a829-000d3a1d5713&forceUCI=1&pagetype=entitylist&etn=invoice&viewid=344d1185-a122-4cc3-b64b-869f938efb17&viewType=1039";
        }

        var fnOnClickRecentInvoice = function () {
            parent.location = clientUrl + "/main.aspx?appid=27ba11a2-425d-e911-a829-000d3a1d5713&forceUCI=1&pagetype=entitylist&etn=invoice&viewid=e186e469-aabb-e611-80d6-00155db0be3a&viewType=1039";
        }

        var fnOnClickClosedInvoice = function () {
            parent.location = clientUrl + "/main.aspx?appid=27ba11a2-425d-e911-a829-000d3a1d5713&forceUCI=1&pagetype=entitylist&etn=invoice&viewid=812c3a80-3f34-405b-b807-516173149340&viewType=1039";
        }



        return {
            OnLoadCommercial: fcOnLoadCommercial,
            OnClickCreateOpportunit: fnOnClickCreateOpportunit,
            OnClickManageOpportunities: fnOnClickManageOpportunities,
            OnClickCreateQuote: fnOnClickCreateQuote,
            OnClickManageQuote: fnOnClickManageQuote,
            OnClickCreateOrders: fnOnClickCreateOrders,
            OnClickManageOrders: fnOnClickManageOrders,
            OnClickAllInvoices: fnOnClickAllInvoices,
            OnClickRecentInvoice: fnOnClickRecentInvoice,
            OnClickClosedInvoice: fnOnClickClosedInvoice
        };
    }());

}