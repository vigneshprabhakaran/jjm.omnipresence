﻿var eventManagement;
if (eventManagement === undefined || eventManagement === null) {
    eventManagement = {};
    eventManagement = (function () {

        var clientUrl;
        var userSettings;
        function PopulateChart(lables, dataSet, ctx) {

            var myPieChart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    labels: lables,
                    datasets: [{
                        data: dataSet,
                        backgroundColor: ['#4e73df', '#1cc88a', '#CC0000','#F1C232'],
                        hoverBackgroundColor: ['#2e59d9', '#A0DAC5', '#E57D7D','#F9E6AB'],
                        hoverBorderColor: "rgba(234, 236, 244, 1)",
                    }],
                },
                options: {
                    maintainAspectRatio: false,
                    legend: {
                        display: true
                    },
                    cutoutPercentage: 1,
                },
            });
        }

        function CallDashboardDataAction() {
            var req = new XMLHttpRequest();
            req.open("POST", parent.Xrm.Page.context.getClientUrl() + "/api/data/v9.1/indskr_iOGetEventManagementDashboardDetails", true);
            req.setRequestHeader("OData-MaxVersion", "4.0");
            req.setRequestHeader("OData-Version", "4.0");
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.onreadystatechange = function () {
                if (this.readyState === 4) {
                    req.onreadystatechange = null;
                    if (this.status === 200) {
                        var results = JSON.parse(this.response);
                        PopulateNumbers(results);
                    } else {
                        parent.Xrm.Utility.alertDialog(this.statusText);
                    }
                }
            };
            req.send();
        }

        function PopulateNumbers(results) {
            var result = JSON.parse(results.JSONOutput);
            EventPlanChart(result);
            EventsCharts(result);
            SpeakerCharts(result);
            SpeakerEngagementCharts(result);
            SpeakerContractCharts(result);

            $("#Registrations").text(result.RegistrationNumbers);
            $("#LeadsCaptured").text(result.LeadsCapturedNumbers);

            $("#Approved").text(result.SpeakerEngagementsCount);
            $("#Confirmed").text(result.VenuesConfirmedNumbers);
            $("#Registered").text(result.VendorsRegistrationNumbers);


            parent.Xrm.Utility.closeProgressIndicator();
        }

        function EventPlanChart(result) {
            var data = result.EventPlanCategories;
            var catagory = [];
            var count = [];
            for (var i = 0; i < data.length; i++) {
                catagory.push(data[i].Status);
                count.push(data[i].Count);
            }
            var ctx = document.getElementById("EventPlanChart");
            PopulateChart(catagory, count, ctx);
            $("#EventPlanCategoriesCount").text(result.EventPlanCategoriesCount);
        }

        function EventsCharts(result) {
            var data = result.EventsCategories;
            var catagory = [];
            var count = [];
            for (var i = 0; i < data.length; i++) {
                catagory.push(data[i].Status);
                count.push(data[i].Count);
            }
            var ctx = document.getElementById("EventsChart");
            PopulateChart(catagory, count, ctx);
            $("#EventsCategoriesCount").text(result.EventsCategoriesCount);
        }

        function SpeakerCharts(result) {
            var data = result.SpeakerCategories;
            var catagory = [];
            var count = [];
            for (var i = 0; i < data.length; i++) {
                catagory.push(data[i].Status);
                count.push(data[i].Count);
            }
            var ctx = document.getElementById("SpeakerCharts");
            PopulateChart(catagory, count, ctx);
            $("#SpeakerCategoriesCount").text(result.SpeakerCategoriesCount);
        }

        function SpeakerEngagementCharts(result) {
            var data = result.SpeakerEngagements;
            var catagory = [];
            var count = [];
            for (var i = 0; i < data.length; i++) {
                catagory.push(data[i].Status);
                count.push(data[i].Count);
            }
            var ctx = document.getElementById("SpeakerEngagementCharts");
            PopulateChart(catagory, count, ctx);
            $("#SpeakerEngagementsCount").text(result.SpeakerEngagementsCount);
        }

        function SpeakerContractCharts(result) {
            var data = result.SpeakerContracts;
            var catagory = [];
            var count = [];
            for (var i = 0; i < data.length; i++) {
                catagory.push(data[i].Status);
                count.push(data[i].Count);
            }
            var ctx = document.getElementById("SpeakerContractCharts");
            PopulateChart(catagory, count, ctx);
            $("#SpeakerContractsCount").text(result.SpeakerContractsCount);
        }


        var fnOnLoadEventDashboard = function () {
            var globalContext = parent.Xrm.Utility.getGlobalContext();
            clientUrl = globalContext.getClientUrl();
            userSettings = globalContext.userSettings;
            $("#username").text(userSettings.userName);
            CallDashboardDataAction();
        };

        var fnOnClickCreatePlan = function () {
            parent.Xrm.Utility.openEntityForm("ind_eventplan");
        }

        var fnOnClicCreateEvent = function () {
            parent.Xrm.Utility.openEntityForm("msevtmgt_event");
        }

        var fnOnClickManagePlan = function () {
            parent.location = clientUrl + "/main.aspx?appid=6aee9f50-00fd-e911-a833-000d3a1d5de8&forceUCI=1&newWindow=true&pagetype=entitylist&etn=ind_eventplan&viewid=ef4f8cf1-3011-4287-9e17-0008fc2c0ed1&viewType=1039";
        }

        var fnOnClickManageEvents = function () {
            parent.location = clientUrl + "/main.aspx?appid=6aee9f50-00fd-e911-a833-000d3a1d5de8&forceUCI=1&newWindow=true&pagetype=entitylist&etn=msevtmgt_event&viewid=90c6947a-73d4-46f8-a1b9-658512390ac7&viewType=1039";
        }

        var fnOnClickSpeakers = function () {
            parent.location = clientUrl + "/main.aspx?appid=6aee9f50-00fd-e911-a833-000d3a1d5de8&forceUCI=1&newWindow=true&pagetype=entitylist&etn=msevtmgt_speaker&viewid=906f8b9a-cfc2-43c6-b499-8bd804041e6b&viewType=1039";
        }

        var fnOnClickEngagements = function () {
            parent.location = clientUrl + "/main.aspx?appid=6aee9f50-00fd-e911-a833-000d3a1d5de8&forceUCI=1&newWindow=true&pagetype=entitylist&etn=msevtmgt_speakerengagement&viewid=BDBE2033-4399-4D49-A428-302AF2DD9349&viewType=1039";
        }

        var fnOnClickContracts = function () {
            parent.location = clientUrl + "/main.aspx?appid=6aee9f50-00fd-e911-a833-000d3a1d5de8&forceUCI=1&newWindow=true&pagetype=entitylist&etn=indskr_speakercontract&viewid=6f93562d-cd7c-4a38-ab68-a6a932ff4a07&viewType=1039";
        }

        var fnOnClickRegistrations = function () {
            parent.location = clientUrl + "/main.aspx?appid=6aee9f50-00fd-e911-a833-000d3a1d5de8&forceUCI=1&newWindow=true&pagetype=entitylist&etn=msevtmgt_registrationresponse&viewid=c9f47971-17d9-4dfb-94f3-830b470693ba&viewType=1039";
        }

        var fnOnClickLeads  = function () {
            parent.location = clientUrl + "/main.aspx?appid=6aee9f50-00fd-e911-a833-000d3a1d5de8&forceUCI=1&newWindow=true&pagetype=entitylist&etn=lead&viewid=72D92E34-AAA2-EA11-A812-000D3A99BA4C&viewType=1039"; 
        }

        var fnOnClickSessions = function () {
            parent.location = clientUrl + "/main.aspx?appid=6aee9f50-00fd-e911-a833-000d3a1d5de8&forceUCI=1&newWindow=true&pagetype=entitylist&etn=msevtmgt_session&viewid=0969db91-8faa-482c-b745-fb244cd5513d&viewType=1039";
        }

        var fnOnClickVenues = function () {
            parent.location = clientUrl + "/main.aspx?appid=6aee9f50-00fd-e911-a833-000d3a1d5de8&forceUCI=1&newWindow=true&pagetype=entitylist&etn=msevtmgt_venue&viewid=f7ca9361-06cc-457d-8445-6153f928d632&viewType=1039";
        }

        var fnOnClickVendors = function () {
            parent.location = clientUrl + "/main.aspx?appid=6aee9f50-00fd-e911-a833-000d3a1d5de8&forceUCI=1&newWindow=true&pagetype=entitylist&etn=ind_vendorcontract&viewid=57ca8cec-e7dc-4051-9090-9691a4d99a38&viewType=1039";
        }




        return {
            OnLoadEventDashboard: fnOnLoadEventDashboard,
            OnClickCreatePlan: fnOnClickCreatePlan,
            OnClicCreateEvent: fnOnClicCreateEvent,
            OnClickManagePlan: fnOnClickManagePlan,
            OnClickManageEvents: fnOnClickManageEvents,
            OnClickSpeakers: fnOnClickSpeakers,
            OnClickEngagements: fnOnClickEngagements,
            OnClickContracts: fnOnClickContracts,
            OnClickRegistrations: fnOnClickRegistrations,
            OnClickLeads: fnOnClickLeads,
            OnClickSessions: fnOnClickSessions,
            OnClickVenues: fnOnClickVenues,
            OnClickVendors: fnOnClickVendors
        };
    }());

}