﻿var workorder;
var xrmPage = "";
if (workorder === undefined || workorder === null) {
    workorder = {};
    workorder = (function () {

        var fnOnLoadWorkOrder = function (executionContext) {
            xrmPage = executionContext.getFormContext();
            xrmPage.data.process.addOnStageSelected(FocusOnTab);
        };
        var FocusOnTab = function () {
            var selectedStage = xrmPage.data.process.getSelectedStage();
            if (selectedStage !== null) {
                // GUID of Installation stage 
                if (selectedStage.getName() === "d83ac466-e24a-4af1-a4b4-f7fcd803307d") {
                    xrmPage.ui.tabs.get("tab_2").setFocus();
                }
            }
        };

        return {
            onLoadWorkOrder: fnOnLoadWorkOrder

        };
    }());

}