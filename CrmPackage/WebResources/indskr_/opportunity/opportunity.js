﻿var opportunity;
var xrmPage = "";
if (opportunity === undefined || opportunity === null) {
    opportunity = {};
    opportunity = (function () {

        var fnOnLoadopportunity = function (context) {
            xrmPage = context.getFormContext();
            xrmPage.data.process.addOnStageSelected(FocusOnTab);
        };

        var FocusOnTab = function () {
            var selectedStage = xrmPage.data.process.getSelectedStage();
            if (selectedStage !== null) {
                // GUID of Create Quote stage
                if (selectedStage.getId() === "d72dad91-4e06-47a7-80c5-58f6c4c875a6") {
                    xrmPage.ui.tabs.get("QUOTES").setFocus();
                }
                // GUID of Site Review and Infosec Review stages
                else if (selectedStage.getId() === "404a138c-de64-43d1-b234-90c145b2dbb0" || selectedStage.getId() === "9435fb81-77c5-4ea7-8f4f-618e57fc3311") {
                    xrmPage.ui.tabs.get("tab_5").setFocus();
                }
            }
        };

        return {
            onLoadOpportunity: fnOnLoadopportunity

        };
    }());

}