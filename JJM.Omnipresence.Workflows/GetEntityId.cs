﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace JJM.Omnipresence.Workflows
{
    public class GetEntityId : WorkFlowActivityBase
    {
        [Input("Contact")]
        [ReferenceTarget("contact")]
        public InArgument<EntityReference> Contact { get; set; }

        [Input("KnowledgeArticle")]
        [ReferenceTarget("knowledgearticle")]
        public InArgument<EntityReference> KnowledgeArticle { get; set; }

        [Input("CustomerPosition")]
        [ReferenceTarget("indskr_customerposition")]
        public InArgument<EntityReference> CustomerPosition { get; set; }

        [Input("CustomerEmail")]
        [ReferenceTarget("indskr_email_address")]
        public InArgument<EntityReference> CustomerEmail { get; set; }

        [Input("CustomerAddress")]
        [ReferenceTarget("indskr_indskr_customeraddress_v2")]
        public InArgument<EntityReference> CustomerAddress { get; set; }

        [Input("AccountContactAffiliation")]
        [ReferenceTarget("indskr_accountcontactaffiliation")]
        public InArgument<EntityReference> AccountContactAffiliation { get; set; }

        [Input("Account")]
        [ReferenceTarget("account")]
        public InArgument<EntityReference> Account { get; set; }

        [Output("EntityId")]
        public OutArgument<string> EntityId { get; set; }

        public override void ExecuteCRMWorkFlowActivity(CodeActivityContext executionContext, LocalWorkflowContext crmWorkflowContext)
        {

            if (crmWorkflowContext == null)
            {
                throw new ArgumentNullException("crmWorkflowContext");
            }

            try
            {
                crmWorkflowContext.Trace("Method: " + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);

                IWorkflowContext context = executionContext.GetExtension<IWorkflowContext>();

                EntityReference contact = Contact.Get(executionContext);
                EntityReference customerPosition = CustomerPosition.Get(executionContext);
                EntityReference customerEmail = CustomerEmail.Get(executionContext);
                EntityReference customerAddress = CustomerAddress.Get(executionContext);
                EntityReference contactAccountAffiliation = AccountContactAffiliation.Get(executionContext);
                EntityReference account = Account.Get(executionContext);
                EntityReference knowledgeArticle = KnowledgeArticle.Get(executionContext);

                if (contact != null)
                    EntityId.Set(executionContext, contact.Id.ToString());
                else if (customerPosition != null)
                    EntityId.Set(executionContext, customerPosition.Id.ToString());
                else if (customerEmail != null)
                    EntityId.Set(executionContext, customerEmail.Id.ToString());
                else if (customerAddress != null)
                    EntityId.Set(executionContext, customerAddress.Id.ToString());
                else if (contactAccountAffiliation != null)
                    EntityId.Set(executionContext, contactAccountAffiliation.Id.ToString());
                else if (account != null)
                    EntityId.Set(executionContext, account.Id.ToString());
                else if (knowledgeArticle != null)
                    EntityId.Set(executionContext, knowledgeArticle.Id.ToString());
                //comment
            }
            catch (FaultException<OrganizationServiceFault> e)
            {
                // Handle the exception.
                throw e;
            }
        }
    }
}
