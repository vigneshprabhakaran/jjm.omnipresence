﻿// <copyright file="GetEventManagementDashboardDetails.cs" company="">
// Copyright (c) 2020 All Rights Reserved
// </copyright>
// <author></author>
// <date>5/28/2020 11:43:16 AM</date>
// <summary>Implements the GetEventManagementDashboardDetails Workflow Activity.</summary>
namespace JJM.Omnipresence.Workflows
{
    using System;
    using System.Activities;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Json;
    using System.ServiceModel;
    using System.Text;
    using System.Xml;
    using Microsoft.Xrm.Sdk;
    using Microsoft.Xrm.Sdk.Messages;
    using Microsoft.Xrm.Sdk.Metadata;
    using Microsoft.Xrm.Sdk.Query;
    using Microsoft.Xrm.Sdk.Workflow;

    public sealed class GetEventManagementDashboardDetails : CodeActivity
    {

        [Output("JSONOutput")]
        [Default("")]
        public OutArgument<string> JSONOutput { get; set; }

        /// <summary>
        /// Executes the workflow activity.
        /// </summary>
        /// <param name="executionContext">The execution context.</param>
        protected override void Execute(CodeActivityContext executionContext)
        {
            // Create the tracing service
            ITracingService tracingService = executionContext.GetExtension<ITracingService>();

            if (tracingService == null)
            {
                throw new InvalidPluginExecutionException("Failed to retrieve tracing service.");
            }

            tracingService.Trace("Entered GetEventManagementDashboardDetails.Execute(), Activity Instance Id: {0}, Workflow Instance Id: {1}",
                executionContext.ActivityInstanceId,
                executionContext.WorkflowInstanceId);

            // Create the context
            IWorkflowContext context = executionContext.GetExtension<IWorkflowContext>();

            if (context == null)
            {
                throw new InvalidPluginExecutionException("Failed to retrieve workflow context.");
            }

            tracingService.Trace("GetEventManagementDashboardDetails.Execute(), Correlation Id: {0}, Initiating User: {1}",
                context.CorrelationId,
                context.InitiatingUserId);

            IOrganizationServiceFactory serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);

            try
            {
                tracingService.Trace("Starting workflow");

                JSONOutput.Set(executionContext, SerializeObjectToJSON(GetEventOverviewNumbers(service, tracingService)));
            }
            catch (FaultException<OrganizationServiceFault> e)
            {
                tracingService.Trace("Exception: {0}", e.ToString());

                // Handle the exception.
                throw;
            }

            tracingService.Trace("Exiting GetEventManagementDashboardDetails.Execute(), Correlation Id: {0}", context.CorrelationId);
        }

        private EventDashboardResponse GetEventOverviewNumbers(IOrganizationService service, ITracingService tracingService)
        {
            EventDashboardResponse response = new EventDashboardResponse();

            response = GetEventsNumbers(service, tracingService, response);

            response = GetEventPlanNumbers(service, tracingService, response);

            response = GetSpeakerNumbers(service, tracingService, response);

            response = GetSpeakerEngagementNumbers(service, tracingService, response);

            response = GetSpeakerContractNumbers(service, tracingService, response);

            response = GetRegistrationNumbers(service, tracingService, response);

            response = GetLeadNumbers(service, tracingService, response);

            response = GetSessionApprovedNumbers(service, tracingService, response);

            response = GetVenuesConfirmedNumbers(service, tracingService, response);

            response = GetVendorsRegistrationNumbers(service, tracingService, response);

            return response;
        }

        private EventDashboardResponse GetVendorsRegistrationNumbers(IOrganizationService service, ITracingService tracingService, EventDashboardResponse response)
        {
            string fetchXml = @"<fetch>
                                  <entity name='ind_vendorcontract' />
                                </fetch>";

            var fetchQuery = new FetchExpression(fetchXml);
            EntityCollection results = service.RetrieveMultiple(fetchQuery);

            tracingService.Trace($"GetVendorsRegistrationNumbers Count: {results.Entities.Count()}");

            tracingService.Trace($"GetVenuesConfirmedNumbers Count: {results.Entities.Count()}");

            response.VendorsRegistrationNumbers = results.Entities.Count();

            return response;
        }

        private EventDashboardResponse GetVenuesConfirmedNumbers(IOrganizationService service, ITracingService tracingService, EventDashboardResponse response)
        {
            var QEmsevtmgt_venue = new QueryExpression("msevtmgt_venue");

            QEmsevtmgt_venue.ColumnSet.AllColumns = true;
            EntityCollection results = service.RetrieveMultiple(QEmsevtmgt_venue);

            tracingService.Trace($"GetVenuesConfirmedNumbers Count: {results.Entities.Count()}");

            response.VenuesConfirmedNumbers = results.Entities.Count();

            return response;
        }

        private EventDashboardResponse GetSessionApprovedNumbers(IOrganizationService service, ITracingService tracingService, EventDashboardResponse response)
        {
            string fetchXml = @"<fetch aggregate='true' >
                                  <entity name='msevtmgt_session' >
                                    <attribute name='indskr_approvalstatus' alias='Count' aggregate='count' />
                                    <attribute name='indskr_approvalstatus' groupby='true' alias='Status' />
                                    <filter>
                                      <condition attribute='indskr_approvalstatus' operator='eq' value='548910002' />
                                    </filter>
                                  </entity>
                                </fetch>";

            var fetchQuery = new FetchExpression(fetchXml);
            EntityCollection results = service.RetrieveMultiple(fetchQuery);

            tracingService.Trace($"GetSessionApprovedNumbers Count: {results.Entities.Count()}");

            foreach (var entity in results.Entities)
            {
                int totalAmount = (Int32)(((AliasedValue)entity["Count"]).Value);
                response.SessionApprovedNumbers = totalAmount;
            }
            return response;
        }

        private EventDashboardResponse GetLeadNumbers(IOrganizationService service, ITracingService tracingService, EventDashboardResponse response)
        {
            string fetchXml = @"<fetch>
                                  <entity name='lead' >
                                    <filter>
                                      <condition attribute='msevtmgt_originatingeventid' operator='not-null' />
                                    </filter>
                                  </entity>
                                </fetch>";

            var fetchQuery = new FetchExpression(fetchXml);
            EntityCollection results = service.RetrieveMultiple(fetchQuery);

            tracingService.Trace($"GetLeadNumbers Count: {results.Entities.Count()}");

            response.LeadsCapturedNumbers = results.Entities.Count();
           
            return response;
        }

        private EventDashboardResponse GetRegistrationNumbers(IOrganizationService service, ITracingService tracingService, EventDashboardResponse response)
        {
            string fetchXml = @"<fetch aggregate='true' >
                                  <entity name='msevtmgt_registrationresponse' >
                                    <attribute name='statecode' alias='Count' aggregate='count' />
                                    <attribute name='statecode' groupby='true' alias='Status' />
                                  </entity>
                                </fetch>";

            var fetchQuery = new FetchExpression(fetchXml);
            EntityCollection results = service.RetrieveMultiple(fetchQuery);

            tracingService.Trace($"GetRegistrationNumbers Count: {results.Entities.Count()}");

            foreach (var entity in results.Entities)
            {
                int totalAmount = (Int32)(((AliasedValue)entity["Count"]).Value);
                response.RegistrationNumbers = totalAmount;
            }
            return response;
        }

        private EventDashboardResponse GetEventsNumbers(IOrganizationService service, ITracingService tracingService, EventDashboardResponse response)
        {
            string fetchXml = @"<fetch aggregate='true' >
                                  <entity name='msevtmgt_event' >
                                    <attribute name='statuscode' alias='Count' aggregate='count' />
                                    <attribute name='statuscode' groupby='true' alias='Status' />
                                  </entity>
                                </fetch>";

            var fetchQuery = new FetchExpression(fetchXml);
            EntityCollection results = service.RetrieveMultiple(fetchQuery);

            tracingService.Trace($"GetEventsNumbers Count: {results.Entities.Count()}");

            List<EventsCatagory> eventsCatagory = new List<EventsCatagory>();

            int totalRecordCount = 0;
            foreach (var entity in results.Entities)
            {
                EventsCatagory eCatagory = new EventsCatagory();
                int totalAmount = (Int32)(((AliasedValue)entity["Count"]).Value);
                eCatagory.Count = totalAmount;
                totalRecordCount = totalRecordCount + totalAmount;
                int setValue = ((OptionSetValue)((AliasedValue)entity["Status"]).Value).Value;
                eCatagory.Status = GetOptionSetValueLabel("msevtmgt_event", "statuscode", setValue, service);
                eventsCatagory.Add(eCatagory);
            }
            response.EventsCategories = eventsCatagory;
            response.EventsCategoriesCount = totalRecordCount;


            return response;
        }

        private EventDashboardResponse GetEventPlanNumbers(IOrganizationService service, ITracingService tracingService, EventDashboardResponse response)
        {
            string fetchXml = @"<fetch aggregate='true' >
                                  <entity name='ind_eventplan' >
                                    <attribute name='statuscode' alias='Count' aggregate='count' />
                                    <attribute name='statuscode' groupby='true' alias='Status' />
                                  </entity>
                                </fetch>";

            var fetchQuery = new FetchExpression(fetchXml);
            EntityCollection results = service.RetrieveMultiple(fetchQuery);

            tracingService.Trace($"GetEventPlanNumbers Count: {results.Entities.Count()}");

            List<EventPlanCatagory> eventPlanCatagory = new List<EventPlanCatagory>();
            int totalRecordCount = 0;
            foreach (var entity in results.Entities)
            {
                EventPlanCatagory eCatagory = new EventPlanCatagory();
                int totalAmount = (Int32)(((AliasedValue)entity["Count"]).Value);
                eCatagory.Count = totalAmount;
                totalRecordCount = totalRecordCount + totalAmount;
                int setValue = ((OptionSetValue)((AliasedValue)entity["Status"]).Value).Value;
                eCatagory.Status = GetOptionSetValueLabel("ind_eventplan", "statuscode", setValue, service);
                eventPlanCatagory.Add(eCatagory);
            }
            response.EventPlanCategories = eventPlanCatagory;
            response.EventPlanCategoriesCount = totalRecordCount;

            return response;
        }

        private EventDashboardResponse GetSpeakerNumbers(IOrganizationService service, ITracingService tracingService, EventDashboardResponse response)
        {
            string fetchXml = @"<fetch aggregate='true' >
                                  <entity name='msevtmgt_speaker' >
                                    <attribute name='indskr_approvalstatus' alias='Count' aggregate='count' />
                                    <attribute name='indskr_approvalstatus' groupby='true' alias='Status' />
                                  </entity>
                                </fetch>";

            var fetchQuery = new FetchExpression(fetchXml);
            EntityCollection results = service.RetrieveMultiple(fetchQuery);

            tracingService.Trace($"GetSpeakerNumbers Count: {results.Entities.Count()}");

            List<SpeakerCatagory> speakerCatagory = new List<SpeakerCatagory>();
            int totalRecordCount = 0;
            foreach (var entity in results.Entities)
            {
                SpeakerCatagory eCatagory = new SpeakerCatagory();
                int totalAmount = (Int32)(((AliasedValue)entity["Count"]).Value);
                eCatagory.Count = totalAmount;
                totalRecordCount = totalRecordCount + totalAmount;
                int setValue = ((OptionSetValue)((AliasedValue)entity["Status"]).Value).Value;
                eCatagory.Status = GetOptionSetValueLabel("msevtmgt_speaker", "indskr_approvalstatus", setValue, service);
                speakerCatagory.Add(eCatagory);
            }
            response.SpeakerCategories = speakerCatagory;
            response.SpeakerCategoriesCount = totalRecordCount;
            return response;
        }

        private EventDashboardResponse GetSpeakerEngagementNumbers(IOrganizationService service, ITracingService tracingService, EventDashboardResponse response)
        {
            string fetchXml = @"<fetch aggregate='true' >
                                  <entity name='msevtmgt_speakerengagement' >
                                    <attribute name='indskr_approvalstatus' alias='Count' aggregate='count' />
                                    <attribute name='indskr_approvalstatus' groupby='true' alias='Status' />
                                  </entity>
                                </fetch>";

            var fetchQuery = new FetchExpression(fetchXml);
            EntityCollection results = service.RetrieveMultiple(fetchQuery);

            tracingService.Trace($"GetSpeakerEngagementNumbers Count: {results.Entities.Count()}");

            List<SpeakerEngagement> speakerEngagements = new List<SpeakerEngagement>();
            int totalRecordCount = 0;
            foreach (var entity in results.Entities)
            {
                SpeakerEngagement eCatagory = new SpeakerEngagement();
                int totalAmount = (Int32)(((AliasedValue)entity["Count"]).Value);
                eCatagory.Count = totalAmount;
                totalRecordCount = totalRecordCount + totalAmount;
                int setValue = ((OptionSetValue)((AliasedValue)entity["Status"]).Value).Value;
                eCatagory.Status = GetOptionSetValueLabel("msevtmgt_speakerengagement", "indskr_approvalstatus", setValue, service);
                speakerEngagements.Add(eCatagory);
            }
            response.SpeakerEngagements = speakerEngagements;
            response.SpeakerEngagementsCount = totalRecordCount;
            return response;
        }

        private EventDashboardResponse GetSpeakerContractNumbers(IOrganizationService service, ITracingService tracingService, EventDashboardResponse response)
        {
            string fetchXml = @"<fetch aggregate='true' >
                                  <entity name='indskr_speakercontract' >
                                    <attribute name='indskr_approvalstatus' alias='Count' aggregate='count' />
                                    <attribute name='indskr_approvalstatus' groupby='true' alias='Status' />
                                  </entity>
                                </fetch>";

            var fetchQuery = new FetchExpression(fetchXml);
            EntityCollection results = service.RetrieveMultiple(fetchQuery);

            tracingService.Trace($"GetSpeakerContractNumbers Count: {results.Entities.Count()}");

            List<SpeakerContract> speakerContract = new List<SpeakerContract>();
            int totalRecordCount = 0;
            foreach (var entity in results.Entities)
            {
                SpeakerContract sCatagory = new SpeakerContract();
                int totalAmount = (Int32)(((AliasedValue)entity["Count"]).Value);
                sCatagory.Count = totalAmount;
                totalRecordCount = totalRecordCount + totalAmount;
                int setValue = ((OptionSetValue)((AliasedValue)entity["Status"]).Value).Value;
                sCatagory.Status = GetOptionSetValueLabel("msevtmgt_speaker", "indskr_approvalstatus", setValue, service);
                speakerContract.Add(sCatagory);
            }
            response.SpeakerContracts = speakerContract;
            response.SpeakerContractsCount = totalRecordCount;
            return response;
        }

        private string SerializeObjectToJSON(object obj)
        {
            DataContractJsonSerializer json = new DataContractJsonSerializer(obj.GetType());
            MemoryStream ms = new MemoryStream();
            XmlDictionaryWriter writer = JsonReaderWriterFactory.CreateJsonWriter(ms);
            json.WriteObject(ms, obj);
            writer.Flush();
            return Encoding.UTF8.GetString(ms.GetBuffer()).Trim().TrimEnd('\0');
        }

        private string GetOptionSetValueLabel(string entityName, string fieldName, int optionSetValue, IOrganizationService service)
        {
            var attReq = new RetrieveAttributeRequest();
            attReq.EntityLogicalName = entityName;
            attReq.LogicalName = fieldName;
            attReq.RetrieveAsIfPublished = true;

            var attResponse = (RetrieveAttributeResponse)service.Execute(attReq);
            var attMetadata = (EnumAttributeMetadata)attResponse.AttributeMetadata;

            return attMetadata.OptionSet.Options.Where(x => x.Value == optionSetValue).FirstOrDefault().Label.UserLocalizedLabel.Label;
        }
    }
    [DataContract]
    internal class EventDashboardResponse
    {
        [DataMember]
        public List<EventsCatagory> EventsCategories  { get; set; }

        [DataMember]
        public List<EventPlanCatagory> EventPlanCategories { get; set; }

        [DataMember]
        public List<SpeakerCatagory> SpeakerCategories { get; set; }

        [DataMember]
        public List<SpeakerEngagement> SpeakerEngagements { get; set; }

        [DataMember]
        public List<SpeakerContract> SpeakerContracts { get; set; }

        [DataMember]
        public int EventsCategoriesCount { get; set; }

        [DataMember]
        public int EventPlanCategoriesCount { get; set; }

        [DataMember]
        public int SpeakerCategoriesCount { get; set; }

        [DataMember]
        public int SpeakerEngagementsCount { get; set; }

        [DataMember]
        public int SpeakerContractsCount { get; set; }

        [DataMember]
        public int RegistrationNumbers { get; set; }

        [DataMember]
        public int LeadsCapturedNumbers { get; set; }

        [DataMember]
        public int SessionApprovedNumbers { get; set; }

        [DataMember]
        public int VenuesConfirmedNumbers { get; set; }

        [DataMember]
        public int VendorsRegistrationNumbers { get; set; }

    }
    [DataContract]
    public class EventsCatagory
    {
        [DataMember]
        public int Count { get; set; }
        [DataMember]
        public string Status { get; set; }
    }

    [DataContract]
    public class EventPlanCatagory
    {
        [DataMember]
        public int Count { get; set; }
        [DataMember]
        public string Status { get; set; }
    }

    [DataContract]
    public class SpeakerCatagory
    {
        [DataMember]
        public int Count { get; set; }
        [DataMember]
        public string Status { get; set; }
    }

    [DataContract]
    public class SpeakerEngagement
    {
        [DataMember]
        public int Count { get; set; }
        [DataMember]
        public string Status { get; set; }
    }

    [DataContract]
    public class SpeakerContract
    {
        [DataMember]
        public int Count { get; set; }
        [DataMember]
        public string Status { get; set; }
    }

    

}