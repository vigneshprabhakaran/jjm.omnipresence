﻿// <copyright file="GetExpertEngagementsDashboardDetails.cs" company="">
// Copyright (c) 2020 All Rights Reserved
// </copyright>
// <author></author>
// <date>5/25/2020 1:11:08 AM</date>
// <summary>Implements the GetExpertEngagementsDashboardDetails Workflow Activity.</summary>
namespace JJM.Omnipresence.Workflows
{
    using System;
    using System.Activities;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Json;
    using System.ServiceModel;
    using System.Text;
    using System.Xml;
    using Microsoft.Xrm.Sdk;
    using Microsoft.Xrm.Sdk.Messages;
    using Microsoft.Xrm.Sdk.Metadata;
    using Microsoft.Xrm.Sdk.Query;
    using Microsoft.Xrm.Sdk.Workflow;

    public sealed class GetExpertEngagementsDashboardDetails : CodeActivity
    {

        [Output("JSONOutput")]
        [Default("")]
        public OutArgument<string> JSONOutput { get; set; }


        /// <summary>
        /// Executes the workflow activity.
        /// </summary>
        /// <param name="executionContext">The execution context.</param>
        protected override void Execute(CodeActivityContext executionContext)
        {
            // Create the tracing service
            ITracingService tracingService = executionContext.GetExtension<ITracingService>();

            if (tracingService == null)
            {
                throw new InvalidPluginExecutionException("Failed to retrieve tracing service.");
            }

            tracingService.Trace("Entered GetExpertEngagementsDashboardDetails.Execute(), Activity Instance Id: {0}, Workflow Instance Id: {1}",
                executionContext.ActivityInstanceId,
                executionContext.WorkflowInstanceId);

            // Create the context
            IWorkflowContext context = executionContext.GetExtension<IWorkflowContext>();

            if (context == null)
            {
                throw new InvalidPluginExecutionException("Failed to retrieve workflow context.");
            }

            tracingService.Trace("GetExpertEngagementsDashboardDetails.Execute(), Correlation Id: {0}, Initiating User: {1}",
                context.CorrelationId,
                context.InitiatingUserId);

            IOrganizationServiceFactory serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);

            try
            {
                tracingService.Trace("Starting workflow");

                JSONOutput.Set(executionContext, SerializeObjectToJSON(GetEngagementDashbordNumbers(service, tracingService)));
            }
            catch (FaultException<OrganizationServiceFault> e)
            {
                tracingService.Trace("Exception: {0}", e.ToString());

                // Handle the exception.
                throw;
            }

            tracingService.Trace("Exiting GetExpertEngagementsDashboardDetails.Execute(), Correlation Id: {0}", context.CorrelationId);
        }
        private DashboardResponse GetEngagementDashbordNumbers(IOrganizationService service, ITracingService tracingService)
        {
            DashboardResponse response = new DashboardResponse();

            response = GetEngagementRequestsNumbers(service, tracingService, response);

            response = GetEngagementPlansNumbers(service, tracingService, response);

            response = GetEngagementExpensesNumbers(service, tracingService, response);

            response = GetEngagementPaymentsNumbers(service, tracingService, response);

            return response;
        }

        private DashboardResponse GetEngagementPaymentsNumbers(IOrganizationService service, ITracingService tracingService, DashboardResponse response)
        {
            string fetchXml = @"<fetch aggregate='true' >
                                  <entity name='indskr_expertengagementactivitypayment' >
                                    <attribute name='statuscode' alias='Count' aggregate='count' />
                                    <attribute name='statuscode' groupby='true' alias='Status' />
                                  </entity>
                                </fetch>";

            var fetchQuery = new FetchExpression(fetchXml);
            EntityCollection results = service.RetrieveMultiple(fetchQuery);

            tracingService.Trace($"GetEngagementPaymentsNumbers Count: {results.Entities.Count()}");

            List<PaymentCatagory> paymentCatagory = new List<PaymentCatagory>();

            foreach (var entity in results.Entities)
            {
                PaymentCatagory pCatagory = new PaymentCatagory();
                int totalAmount = (Int32)(((AliasedValue)entity["Count"]).Value);
                pCatagory.Count = totalAmount;
                int setValue = ((OptionSetValue)((AliasedValue)entity["Status"]).Value).Value;
                pCatagory.Status = GetOptionSetValueLabel("indskr_expertengagementactivitypayment", "statuscode", setValue, service);

                tracingService.Trace($"Count : {pCatagory.Count}");
                tracingService.Trace($"Status : {pCatagory.Status}");

                paymentCatagory.Add(pCatagory);
            }
            response.PaymentDetails = paymentCatagory;

            tracingService.Trace($"PaymentDetails : {response.PaymentDetails}");

            return response;
        }

        private string GetOptionSetValueLabel(string entityName, string fieldName, int optionSetValue, IOrganizationService service)
        {
            var attReq = new RetrieveAttributeRequest();
            attReq.EntityLogicalName = entityName;
            attReq.LogicalName = fieldName;
            attReq.RetrieveAsIfPublished = true;

            var attResponse = (RetrieveAttributeResponse)service.Execute(attReq);
            var attMetadata = (EnumAttributeMetadata)attResponse.AttributeMetadata;

            return attMetadata.OptionSet.Options.Where(x => x.Value == optionSetValue).FirstOrDefault().Label.UserLocalizedLabel.Label;
        }

        private DashboardResponse GetEngagementExpensesNumbers(IOrganizationService service, ITracingService tracingService, DashboardResponse response)
        {
            string fetchXml = @"<fetch aggregate='true' >
                                  <entity name='indskr_expense' >
                                    <attribute name='indskr_amount' alias='totalamount_sum' aggregate='sum' />
                                    <attribute name='indskr_expense_category' groupby='true' alias='category' />
                                  </entity>
                                </fetch>";

            var fetchQuery = new FetchExpression(fetchXml);
            EntityCollection results = service.RetrieveMultiple(fetchQuery);

            tracingService.Trace($"GetEngagementExpensesNumbers Count: {results.Entities.Count()}");

            List<ExpertCatagory> expertCatagories = new List<ExpertCatagory>();

            foreach (var entity in results.Entities)
            {
                ExpertCatagory eCatagory = new ExpertCatagory();
                Decimal totalAmount = Math.Round(((Money)((AliasedValue)entity["totalamount_sum"]).Value).Value, 2);
                eCatagory.TotalAmount = totalAmount;
                eCatagory.Catagory = ((EntityReference)((AliasedValue)entity["category"]).Value).Name;

                tracingService.Trace($"TotalAmount : {eCatagory.TotalAmount}");
                tracingService.Trace($"Catagory : {eCatagory.Catagory}");


                expertCatagories.Add(eCatagory);
            }
            response.ExpensesDetails = expertCatagories;

            tracingService.Trace($"ExpensesDetails : {response.ExpensesDetails}");

            return response;
        }

        private DashboardResponse GetEngagementPlansNumbers(IOrganizationService service, ITracingService tracingService, DashboardResponse response)
        {
            var QEindskr_expertcontract = new QueryExpression("indskr_expertcontract");

            QEindskr_expertcontract.ColumnSet.AddColumns("indskr_expertcontractid", "indskr_name", "createdon");
            QEindskr_expertcontract.AddOrder("indskr_name", OrderType.Ascending);

            QEindskr_expertcontract.Criteria.AddCondition("indskr_approvalstatus", ConditionOperator.Equal, "548910001");

            EntityCollection results = service.RetrieveMultiple(QEindskr_expertcontract);

            tracingService.Trace($"expertcontract Count: {results.Entities.Count()}");

            response.ContractsInReview = results.Entities.Count().ToString();

            var QEindskr_expertengagementplan = new QueryExpression("indskr_expertengagementplan");

            QEindskr_expertengagementplan.ColumnSet.AddColumns("indskr_expertengagementplanid", "indskr_name", "createdon");
            QEindskr_expertengagementplan.AddOrder("indskr_name", OrderType.Ascending);

            QEindskr_expertengagementplan.Criteria.AddCondition("indskr_approvalstatus", ConditionOperator.Equal, 548910001);

            results = service.RetrieveMultiple(QEindskr_expertcontract);

            tracingService.Trace($"expertengagementplan Count: {results.Entities.Count()}");

            response.ExpertEngagementPlan = results.Entities.Count().ToString();

            return response;
        }

        private DashboardResponse GetEngagementRequestsNumbers(IOrganizationService service, ITracingService tracingService, DashboardResponse response)
        {
            var QEindskr_expertengagementrequest = new QueryExpression("indskr_expertengagementrequest");

            QEindskr_expertengagementrequest.ColumnSet.AddColumns("statuscode");

            EntityCollection results = service.RetrieveMultiple(QEindskr_expertengagementrequest);

            tracingService.Trace($"expertengagementrequest Count: {results.Entities.Count()}");

            int inDraft = 0, inReview = 0, inActive = 0;

            foreach (var val in results.Entities)
            {
                tracingService.Trace($"Status Value: {((OptionSetValue)val.Attributes["statuscode"]).Value}");
                var statuVal = val.GetAttributeValue<OptionSetValue>("statuscode");

                inDraft = (statuVal.Value.ToString() == "1") ? ++inDraft : inDraft;
                inReview = (statuVal.Value.ToString() == "548910001") ? ++inReview : inReview;
                inActive = (statuVal.Value.ToString() == "548910000") ? ++inActive : inActive;
                tracingService.Trace($"Status Value: {inDraft}");

            }

            response.EngagementRequestsInDraft = inDraft.ToString();
            response.EngagementRequestsInReview = inReview.ToString();
            response.EngagementRequestsActive = inActive.ToString();

            tracingService.Trace($"EngagementRequestsInDraft Count: {response.EngagementRequestsInDraft}");

            tracingService.Trace($"EngagementRequestsInReview Count: {response.EngagementRequestsInReview}");

            tracingService.Trace($"EngagementRequestsActive Count: {response.EngagementRequestsActive}");

            return response;
        }

        private string SerializeObjectToJSON(object obj)
        {
            DataContractJsonSerializer json = new DataContractJsonSerializer(obj.GetType());
            MemoryStream ms = new MemoryStream();
            XmlDictionaryWriter writer = JsonReaderWriterFactory.CreateJsonWriter(ms);
            json.WriteObject(ms, obj);
            writer.Flush();
            return Encoding.UTF8.GetString(ms.GetBuffer()).Trim().TrimEnd('\0');
        }
    }
    [DataContract]
    public class DashboardResponse
    {
        [DataMember]
        public string EngagementRequestsInDraft { get; set; }
        [DataMember]
        public string EngagementRequestsInReview { get; set; }
        [DataMember]
        public string EngagementRequestsActive { get; set; }
        [DataMember]
        public string ContractsInReview { get; set; }
        [DataMember]
        public string ExpertEngagementPlan { get; set; }
        [DataMember]
        public List<ExpertCatagory> ExpensesDetails { get; set; }

        [DataMember]
        public List<PaymentCatagory> PaymentDetails { get; set; }
    }

    [DataContract]
    public class ExpertCatagory
    {
        [DataMember]
        public decimal TotalAmount { get; set; }
        [DataMember]
        public string Catagory { get; set; }
    }

    [DataContract]
    public class PaymentCatagory
    {
        [DataMember]
        public int Count { get; set; }
        [DataMember]
        public string Status { get; set; }
    }
}